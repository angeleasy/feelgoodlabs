import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import store from './store'
import router from './router'

Vue.config.productionTip = false

import * as filters from './filters' // global filters
// register global utility filters.
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
