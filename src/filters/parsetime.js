export function parseTime(value) {
    const val = parseInt(value)
    const time = {
        sec: val % 60,
        min: 0,
        hour: 0
    }
    if (val < 3600) {
        time.min = Math.floor(val / 60)
    } else {
        time.hour = Math.floor(val / 3600)
        let hash = val % 3600
        time.min = Math.floor(hash / 60)
    }

    if (time.sec < 10) time.sec = "0" + time.sec
    if (time.min < 10) time.min = "0" + time.min
    if (time.hour < 10) time.hour = "0" + time.hour

    return time.sec + ":" + time.min + ":" + time.hour
}