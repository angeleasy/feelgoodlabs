import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

class Task {
    constructor(time, title) {
        this.time = time
        this.title = title
    }
}

export default new Vuex.Store({
    state: {
        done: [],
        play: null,
        dialog: false,
        task: null,
        timer: null,
        tasks: []
    },
    mutations: {
        SET_PLAY_STATUS(state, payload) {
            state.play = payload
            localStorage.play = payload
        },
        START_TIMER(state, payload) {
            const currentTime = new Date()
            const taskTime = state.tasks[payload].time
            state.timer = setInterval(() => {
                const seconds = Math.floor((new Date() - currentTime) / 1000)
                state.tasks[payload].time = taskTime + seconds
                localStorage.tasks = JSON.stringify(state.tasks)
            }, 1000)
        },
        STOP_TIMER(state) {
            clearInterval(state.timer)
            state.timer = null
        },
        STOP_TASK(state, payload) {
            if(state.play === payload){
                state.play = null
                localStorage.play = null
            }
            state.done.push(payload)
            localStorage.done = state.done
        },
        SET_DONE(state, payload) {
            payload.split(',').forEach((item) => {
                state.done.push(parseInt(item))
            })
        },
        SET_TASKS(state, payload) {
            state.tasks = JSON.parse(payload)
        },
        ADD_TASK(state, payload) {
            state.tasks.push(payload)
            localStorage.tasks = JSON.stringify(state.tasks)
        },
        DELETE_TASK(state, payload) {
            state.tasks.splice(payload, 1)
            localStorage.tasks = JSON.stringify(state.tasks)

            const stateDone = state.done
            const filtered = stateDone.filter(item => item !== payload)
            state.done = filtered
            localStorage.done = state.done
        },
        UPDATE_TASK(state) {
            localStorage.tasks = JSON.stringify(state.tasks)
        }
    },
    actions: {
        startTimer({ commit }, payload) {
            const i = this.state.play
            const index = payload

            commit('SET_PLAY_STATUS', payload)
            commit('STOP_TIMER')

            if (i === null) {
                commit('START_TIMER', index)
            } else if (i === payload) {
                commit('SET_PLAY_STATUS', null)
            } else {
                commit('START_TIMER', index)
            }
        },
        stopTimer({ commit }) {
            commit('STOP_TIMER')
        },
        stopTask({ commit }, payload) {
            commit('STOP_TASK', payload)
        },
        loadTasks({commit}) {
            if(localStorage.tasks)
                commit('SET_TASKS', localStorage.tasks)

            if(localStorage.done)
                commit('SET_DONE', localStorage.done)

        },
        addTask({commit}, payload) {
            const newTask = new Task(
                0,
                payload
            )
            commit('ADD_TASK', {
                ...newTask
            })
        },
        deleteTask({commit}, payload) {
            commit('STOP_TIMER')
            commit('DELETE_TASK', payload)
            commit('SET_PLAY_STATUS', null)
        },
        updateTask({commit}){
           commit('UPDATE_TASK')
        }
    },
    getters: {
        getTasks: state => state.tasks,
        getTotalTime: state => {
            let total = 0
            state.tasks.forEach((item) => {
                total += item.time
            })
            return total
        },
        getDoneTasks: state => state.done,
        getPlayTask: state => state.play
    }
})
